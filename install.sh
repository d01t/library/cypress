#!/bin/bash
# Stop on error
set -e
# Install mandatory programs and utilities
export DEBIAN_FRONTEND=noninteractive
apt-get --yes --quiet=2 update
apt-get --yes --quiet=2 install \
	apt-transport-https \
	ca-certificates \
	curl \
	dnsutils \
	git \
	libasound2 \
	libgtk2.0-0 \
	libgtk-3-0 \
	libgconf-2-4 \
	libnotify-dev \
	libnss3 \
	libxss1 \
	libxtst6 \
	procps \
	rsync \
	unzip \
	wget \
	xauth \
	xvfb \
	zip
# Purge unnecessary files
rm -rf /var/lib/apt/lists/*


mkdir -p /var/www/html
chown -R node:node /var/www/html
npm install -g cypress --ignore-scripts
su - node -c 'cypress install'
